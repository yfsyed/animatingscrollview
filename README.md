# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

  AnimatingScrollview is a extension to scrollview that can perform a parallax animation of the
  background view associated with parallax container in which scrollview also resides.

* Version 
  0.0.1

### How do I get set up? ###

* Summary of set up:

Add the following maven repository to the module specific build.gradle

```
#!java

repositories {
    maven {
        url  "http://dl.bintray.com/ysyed/maven"
    }
}
```

add the following dependency to the dependencies list in the module specific build.gradle


```
#!java

dependencies {
    compile fileTree(dir: 'libs', include: ['*.jar'])
    compile 'com.yousuf.parallexscrollview:parallexscrollview:0.0.1'
    ...
    ...
}
```


### Configuration ###


 * add the following mandatory fields:
  
        
        app:bannerView="@+id/banner_item"

        app:overlayHeight="@dimen/overlay_height"

        app:scrollView="@+id/parallax_scroll_view"

        app:stickyView="@+id/sticky_header"


  <FrameLayout 
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <com.yousuf.parallexscrollview.ParallaxScrollViewContainer
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        app:backgroundView="@+id/background_banner"
        app:bannerView="@+id/banner_item"
        app:overlayHeight="@dimen/overlay_height"
        app:scrollView="@+id/parallax_scroll_view"
        app:stickyView="@+id/sticky_header">

        <ImageView
            android:id="@+id/background_banner"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:scaleType="fitXY"
            android:src="@drawable/bridge_nature_wallpaper" />

        <com.yousuf.parallexscrollview.ParallaxScroller
            android:id="@+id/parallax_scroll_view"
            android:layout_width="match_parent"
            android:layout_height="match_parent">

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="100dp"
                android:orientation="vertical">

                <View
                    android:layout_width="match_parent"
                    android:layout_height="@dimen/overlay_height"
                    android:background="@android:color/transparent" />

                <LinearLayout
                    android:id="@+id/banner_item"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:background="@android:color/white"
                    android:orientation="horizontal">

                    <include layout="@layout/banner_header" />

                </LinearLayout>

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:background="@android:color/white"
                    android:orientation="vertical">

                    <com.yousuf.parallexscrollview.FeatureLayout
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        app:featureIcon="@mipmap/ic_launcher"
                        app:featureMessage="$200 for current offer"
                        app:featureTitle="Deals" />

                    // list items inside scroll view ....

                    <TextView
                        android:id="@+id/providers"
                        style="@style/FeatureTextStyle"
                        android:layout_marginBottom="4dp"
                        android:background="@color/feature_gold"
                        android:text="some random text" />

                    // list items inside scroll view ....

                </LinearLayout>

            </LinearLayout>

        </com.yousuf.parallexscrollview.ParallaxScroller>

        // if you wish to have a sticky header, do something like this

        <LinearLayout
            android:id="@+id/sticky_header"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@android:color/white"
            android:orientation="horizontal">

            <include layout="@layout/banner_header" />

        </LinearLayout>

    </com.yousuf.parallexscrollview.ParallaxScrollViewContainer>

  </FrameLayout>

* P.S: Frame layout is not needed, its added here to display the main content as code block.


### Who do I talk to? ###

* Repo owner or admin

Myname.yousuf@gmail.com