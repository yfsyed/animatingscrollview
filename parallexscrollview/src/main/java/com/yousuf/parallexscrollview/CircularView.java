package com.yousuf.parallexscrollview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Yousuf Syed on 5/15/15.
 */
public class CircularView extends View {

    private String mText = "";
    private int mTextSize;
    private int mStrokeWidth;

    private int mTextColor = Color.parseColor("#333333");
    private int mBackgroundColor = Color.WHITE;
    private int mStrokeColor = Color.parseColor("#D20962");


    public CircularView(Context context) {
        super(context);
    }

    public CircularView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CircularView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray messageAttributes = context.obtainStyledAttributes(attrs, R.styleable.CircularView);

        String text = messageAttributes.getString(R.styleable.CircularView_text);
        if (text != null) {
            setText(text);
        }

        int textSize = (int) messageAttributes.getDimension(R.styleable.CircularView_textSize,
                                getResources().getDimension(R.dimen.default_text_size));
        if (textSize > 0) {
            setTextSize(textSize);
        }

        int strokeWidth = (int) messageAttributes.getDimension(R.styleable.CircularView_strokeWidth, getResources().getDimension(R.dimen.default_stroke_width));
        if (strokeWidth > 0) {
            setStrokeWidth(strokeWidth);
        }

        int color = messageAttributes.getColor(R.styleable.CircularView_textColor, mTextColor);
        setTextColor(color);

        int strokeColor = messageAttributes.getColor(R.styleable.CircularView_strokeColor, mStrokeColor);
        setStrokeColor(strokeColor);

        int background = messageAttributes.getColor(R.styleable.CircularView_backgroundColor, mBackgroundColor);
        setBackgroundColor(background);

        messageAttributes.recycle();
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int x = getWidth();
        int y = getHeight();

        int centerX = x / 2;
        int centerY = y / 2;

        int radius = centerX - mStrokeWidth;

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(mBackgroundColor);
        canvas.drawCircle(centerX, centerY, radius, paint);

        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(mStrokeColor);
        paint.setStrokeWidth(mStrokeWidth);
        canvas.drawCircle(centerX, centerY, radius, paint);

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(mTextColor);
        paint.setTextSize(mTextSize);
        paint.setAntiAlias(true);
        //Typeface plain = Typeface.createFromAsset(getResources().getAssets(), "Foco_Bd.ttf");

        paint.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        Rect rect = new Rect();
        paint.getTextBounds(mText, 0, mText.length(), rect);

        int textOffsetX = (centerX - rect.width() / 2);
        int textOffsetY = (centerY + rect.height() / 2);

        canvas.drawText(mText, textOffsetX, textOffsetY, paint);


    }

    /**
     * Set Initials to be displayed.
     *
     * @param initials
     */
    public void setText(String initials) {
        mText = initials;
    }

    /**
     * Set Initials Color
     *
     * @param color
     */
    public void setTextColor(int color) {
        mTextColor = color;
    }

    /**
     * Set Text size, defaults to 16sp
     *
     * @param size
     */
    public void setTextSize(int size) {
        mTextSize = size;
    }

    /**
     * Set background color, defaults to white
     *
     * @param color
     */
    public void setBackgroundColor(int color) {
        mBackgroundColor = color;
    }

    /**
     * Set Stroke color
     *
     * @param color
     */
    public void setStrokeColor(int color) {
        mStrokeColor = color;
    }

    /**
     * Set stroke width, default is 5dp
     *
     * @param width
     */
    public void setStrokeWidth(int width) {
        mStrokeWidth = width;
    }

}