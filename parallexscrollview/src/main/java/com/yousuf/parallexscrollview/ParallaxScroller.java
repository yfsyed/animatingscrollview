package com.yousuf.parallexscrollview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

/**
 * Created by Yousuf Syed on 1/31/16.
 */
public class ParallaxScroller extends ScrollView {

    public interface OnScrollChangedListener {
        void onScroll(int left, int top, int oldLeft, int oldTop);
    }

    private OnScrollChangedListener mListener;

    public void setOnScrollChangeCallback(OnScrollChangedListener listener){
        mListener = listener;
    }

    public ParallaxScroller(Context context) {
        super(context);
    }

    public ParallaxScroller(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ParallaxScroller(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void onScrollChanged(int l, int t, int oldl, int oldT){
        super.onScrollChanged(l,t,oldl,oldT);
        if(mListener != null){
            mListener.onScroll(l, t, oldl, oldT);
        }
    }

}
