package com.yousuf.parallexscrollview;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Yousuf Syed on 5/15/15.
 */
public class FeatureLayout extends RelativeLayout {

    public FeatureLayout(Context context) {
        super(context);
    }

    public FeatureLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public FeatureLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        inflate(getContext(), R.layout.feature_layout, this);
        updateCardPadding();

        TypedArray messageAttributes = getContext().obtainStyledAttributes(attrs, R.styleable.FeatureLayout);

        String title = messageAttributes.getString(R.styleable.FeatureLayout_featureTitle);
        if (title != null) {
            setTitle(title);
        }

        String message = messageAttributes.getString(R.styleable.FeatureLayout_featureMessage);
        setMessage(message);

        int featureIcon = messageAttributes.getResourceId(R.styleable.FeatureLayout_featureIcon, R.drawable.circle);
        setFeatureIcon(featureIcon);

        messageAttributes.recycle();
    }

    /**
     * This code is to add padding to card view for API version 21 or higher as
     * we are using support library, without which the cards line up
     * without any padding one below the other on API >= 21.
     */
    protected void updateCardPadding() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ((CardView) findViewById(R.id.feature_card)).setUseCompatPadding(true);
        }
    }

    /**
     * Set Feature title.
     *
     * @param title
     */
    public void setTitle(String title) {
        ((TextView) findViewById(R.id.title)).setText(title);
    }

    /**
     * Set Feature summary.
     *
     * @param message
     */
    public void setMessage(String message) {
        if (!TextUtils.isEmpty(message)) {
            ((TextView) findViewById(R.id.message)).setText(message);
        }
    }

    /**
     * Set Feature icon resource id.
     *
     * @param resId drawable resource id.
     */
    public void setFeatureIcon(int resId) {
        ((ImageView) findViewById(R.id.feature_icon)).setImageResource(resId);
    }

    /**
     * Get feature title.
     *
     * @return feature title
     */
    public String getTitle() {
        return ((TextView) findViewById(R.id.title)).getText().toString();
    }

    /**
     * Get feature summary.
     *
     * @return feature summary.
     */
    public String getMessage() {
        return ((TextView) findViewById(R.id.message)).getText().toString();
    }

}