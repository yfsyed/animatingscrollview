package com.yousuf.parallexscrollview;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Created by Yousuf Syed on 1/31/16.
 */
public class ParallaxScrollViewContainer extends FrameLayout implements ParallaxScroller.OnScrollChangedListener {

    private int mBackgroundViewId, mScrollViewId;

    private View mBackgroundView;

    private ParallaxScroller mScrollView;

    private int mStickyHeaderId, mBannerItemId;  // Optional view ids for sticky header

    private ViewGroup mStickyHeader, mBannerItem; // Optional views for sticky header

    private final int ANIMATION_DURATION = 0;

    private float mDefaultOverlayHeight = 300;

    public ParallaxScrollViewContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ParallaxScrollViewContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attributeSet){
        TypedArray viewAttributes = getContext().obtainStyledAttributes(attributeSet, R.styleable.AnimatingScroller);

        mBackgroundViewId = viewAttributes.getResourceId(R.styleable.AnimatingScroller_backgroundView, 0);
        mStickyHeaderId = viewAttributes.getResourceId(R.styleable.AnimatingScroller_stickyView, 0);
        mScrollViewId = viewAttributes.getResourceId(R.styleable.AnimatingScroller_scrollView, 0);
        mBannerItemId = viewAttributes.getResourceId(R.styleable.AnimatingScroller_bannerView, 0);

        if(mBackgroundViewId ==0 ||  mScrollViewId == 0 ){ //|| mStickyHeaderId == 0 || mBannerItemId == 0 ){
            throw new IllegalArgumentException(viewAttributes.getPositionDescription() + "The required attribute must have valid id");
        }

        mDefaultOverlayHeight = viewAttributes.getDimension(R.styleable.AnimatingScroller_overlayHeight, 300);

        viewAttributes.recycle();
    }

    @Override
    public void onFinishInflate(){
        super.onFinishInflate();
        try {
            mBackgroundView = findViewById(mBackgroundViewId);
            mScrollView = (ParallaxScroller) findViewById(mScrollViewId);
            mScrollView.setFocusable(true);
            mScrollView.setFocusableInTouchMode(true);
            mScrollView.requestFocus();
            mScrollView.setOnScrollChangeCallback(this);

            //Optional parameters
            if(mStickyHeaderId != 0) {
                mStickyHeader = (ViewGroup) findViewById(mStickyHeaderId);
                mStickyHeader.animate().alpha(0).setDuration(0);
            }

            if(mBannerItemId != 0) {
                mBannerItem = (ViewGroup) findViewById(mBannerItemId);
                mBannerItem.setAlpha(0.6f);
            }

        } catch(Exception e){
            throw new RuntimeException("Layout must have child views");
        }

    }

    @Override
    public void onScroll(int left, int top, int oldLeft, int oldTop) {
        animateViews(mScrollView.getScrollY(), ANIMATION_DURATION);
    }

    private void animateViews(float delta, int animDuration){
        if(mStickyHeader != null) {
            if (delta >= mDefaultOverlayHeight) {
                mStickyHeader.animate().alpha(1).setDuration(animDuration).start();
            } else {
                mStickyHeader.animate().alpha(0).setDuration(animDuration).start();
            }
        }

        if(mBannerItem != null) {
            mBannerItem.animate()
                    .alpha(bannerAlphaRatio(delta))
                    .setDuration(animDuration).start();
        }

        delta *= .25;
        mBackgroundView.animate()
                .alpha(alphaRatio(delta))
                .y(-1 * delta).setInterpolator(new LinearOutSlowInInterpolator())
                .setDuration(animDuration).start();
    }

    private float alphaRatio(float delta){
        float height = mBackgroundView.getHeight();
        float alpha = 1;

        if(delta <= height ){
            alpha =  1 - (delta/height);
        }
        return alpha;
    }

    private float bannerAlphaRatio(float delta){
        float height = mDefaultOverlayHeight;
        float alpha = 1;

        if(delta <= height ){
            alpha =  delta/height;

            if(alpha < 0.5){
                alpha = 0.5f;
            }

        }
        return alpha;
    }
}
